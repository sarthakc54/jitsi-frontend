import React, { useState } from 'react'
import {Form,Col,Button} from 'react-bootstrap'
import { Jutsu } from 'react-jutsu'
const App = (props) => {
  const [room, setRoom] = useState(props.room)
  const [name, setName] = useState(props.name)
  //const [call, setCall] = useState()
  const [password, setPassword] = useState(props.password)
  const [subject, setSubject] = useState(props.subject)



  return (
<Jutsu
       containerStyles={{ width: '1200px', height: '800px' }}
       domain='meet.onacall.in'
       subject={subject}
        roomName={room}
        password={password}
        displayName={name}  
        onMeetingEnd={() => console.log('Meeting has ended')}
        loadingComponent={<p> NxtGen Meet is loading ...</p>} />

  )
}

export default App
