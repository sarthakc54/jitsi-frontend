import React, { useState,useEffect } from 'react'
import Jutsu from './jutsu.js'
import {Form,Col,Button,Row} from 'react-bootstrap'
import uuid from "uuid/v4"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {reactLocalStorage} from 'reactjs-localstorage';
import {EmailShareButton} from 'react-share'
import Card from 'react-bootstrap/Card'
var imageName = require('./NxtGen logo.png')
const App = () => {
  useEffect(() => {
    // Update the document title using the browser API
   renderDetails();
  },[]);

  const [room, setRoom] = useState(uuid())
  const [name, setName] = useState('')
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [call, setCall] = useState(false)
  const [password, setPassword] = useState('')
  const [startDate, setStartDate] = useState(new Date());
  const [indents, setIndent]=useState([])
  //reactLocalStorage.set('serialNo', 0);

  const renderDetails=async()=>{
    const indents = [];
    for (var i = 0; i < 5; i++) {
      var details= reactLocalStorage.getObject(i)
      console.log(details)
      if(details.room!=null){
        indents.push(
        <Card style={{margin:"10px"}}>
          <Card.Header as="h5">{details.title}</Card.Header>
          <Card.Body>
          <Card.Title>{details.startDate}</Card.Title>
          <b>Room ID  :</b>{details.room}<br></br>
          <b>HostName :</b>{details.name}<br></br>
          <br></br>
          <Button style={{margin:"20px"}} id={i} variant="success" onClick={handleClick}>Start Now</Button>
          <Button style={{margin:"20px"}} id={i} variant="danger" onClick={removeMeeting}>Remove</Button>
          <EmailShareButton  subject="NxtGen Meet link" body={"Room ID:"+details.room+""}>Share Via Email</EmailShareButton>

          </Card.Body>

          </Card>);
      }

      setIndent(indents);

      }

   // return indents;
  }
  const removeMeeting = (e) => {
  reactLocalStorage.remove(e.target.id)
  renderDetails();
   }
  const handleClick = (e) => {
    var room1 = reactLocalStorage.getObject(e.target.id).room
    var name1 = reactLocalStorage.getObject(e.target.id).name
    console.log(room1)
    console.log(name1)
   // event.preventDefault()
    if (room1 && name1){
      setCall(true)
      setName(name1)
      setRoom(room1)
    }
  }

  const startNow = (event) => {

   event.preventDefault()
    if (room && name){
      setCall(true)

    }
  }
  const createSchedule = event => {
    var serialNo= parseInt(reactLocalStorage.get('serialNo',0,true))
    const time= new Date().getTime()
    event.preventDefault()
    reactLocalStorage.setObject([serialNo], {

        'timeStamp':time,
        'title':title,
        'description':description,
        'room': room,
        'name': name,
        'password':password,
        'startDate':startDate.toLocaleString()



    });
    if(serialNo>=5){
      serialNo=0
    reactLocalStorage.set('serialNo',serialNo+1)

    }
    else{
      reactLocalStorage.set('serialNo',serialNo+1)

    }
    renderDetails();

  }
  return (
    <div style={{display:"flex", flexDirection:"column"}}>
       <h2 style={{alignSelf:"center", padding:"40px"}}>NxtGen WebRTC Demo</h2>



       {call ? (<Jutsu room={room} name={name} pass={password} subject={title}></Jutsu>)
        : (
          <div style={{display:"flex", flexDirection:"row"}}>
<div>
 <Form style={{display:"flex", flexDirection:"column"}}>

    <Form.Group >
      <Form.Label>Room</Form.Label>
      <Form.Control type="text"  placeholder="Enter Room ID" value={room} disabled/>
    </Form.Group>
    <Form.Group  >
      <Form.Label>Meeting Subject</Form.Label>
      <Form.Control type="text"  placeholder="Enter Meeting Title" value={title} onChange={(e) => setTitle(e.target.value)}/>
    </Form.Group>
  <Form.Group  as={Row}>
    <Col><Form.Label>Invitee Name</Form.Label>
      <Form.Control type="text"  placeholder="Enter Your Name" value={name} onChange={(e) => setName(e.target.value)}/>
    </Col>
    <Col>
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password(Optional)" value={password} onChange={(e) => setPassword(e.target.value)}/>
  </Col>
      </Form.Group>
    <Form.Group  >
      <Form.Label>Meeting Discription</Form.Label>
      <Form.Control as="textarea" rows={3}  placeholder="Enter Meeting Description" value={description} onChange={(e) => setDescription(e.target.value)}/>
    </Form.Group>
  <Form.Group >
    <Form.Label>Time</Form.Label>
    <br></br>
    <DatePicker className="form-control" selected={startDate} onChange={date => setStartDate(date)} showTimeSelect dateFormat="MMMM d-h:mm aa"
      />
  </Form.Group>
<Form.Group as={Row}>
<Col>
  <Button variant="primary" onClick={createSchedule} type="submit" style={{marginLeft:"100px"}}>
    Schedule
  </Button>
  </Col>
  <Col>
  <Button variant="success" onClick={startNow} type="submit" >
    Start Now
  </Button>
  </Col>
  </Form.Group>

</Form>

  </div>
  <div style={{width:"600px", marginLeft:"50px"}}>
          {indents}
        </div>

  </div>
        )}



     </div>

  )
}

export default App
